# README #

The REST Client connector provides the flexibility to connect to any RESTful API service, extract the data, and use it in your process. The connector is generic, and is not tailored to any specific REST data source. By being generic, the connector provides a standard way to connect to any RESTful API endpoint service and use the information from the service-specific API, rather than requiring the use of a specific branded connector.

## Projects 

### `:connector`  

Core connector project which packages the connector-sdk-rest library to create a fully-functional connector 

### `:sample:default-headers`

Sample connector project which customizes the connector-sdk-rest library to include default headers in every request 

### `:samples:pet-store`

Sample connector project which customzies the connector-sdk-rest library to support browse and create a simple petstore application connector 

## Build Commands ###

* `gradlew build` - compile the project
* `gradlew release` - release the project 