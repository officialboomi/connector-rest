package com.boomi.connector.sample;

import com.boomi.common.rest.RestConnector;
import com.boomi.common.rest.RestOperation;
import com.boomi.common.rest.RestOperationConnection;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;

/**
 * Simple {@link RestConnector} implementation which uses a custom
 * {@link RestOperation} implementation.
 */
public class CustomRestConnector extends RestConnector {

	@Override
	protected Operation createExecuteOperation(OperationContext context) {
		return new CustomRestOperation(new RestOperationConnection(context));
	}

}
