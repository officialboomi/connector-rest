package com.boomi.connector.sample;

import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

import com.boomi.common.rest.RestOperation;
import com.boomi.common.rest.RestOperationConnection;
import com.boomi.connector.api.ObjectData;
import com.boomi.util.CollectionUtil;

/**
 * Simple {@link RestOperation} implementation which adds default headers to
 * every request.
 */
public class CustomRestOperation extends RestOperation {

	/**
	 * Creates a new instance with the provided connection
	 * 
	 * @param connection
	 */
	public CustomRestOperation(RestOperationConnection connection) {
		super(connection);
	}

	@Override
	protected Iterable<Entry<String, String>> getHeaders(ObjectData data) {
		Set<Entry<String, String>> headers = new HashSet<>();
		// add the user provided headers
		CollectionUtil.addAll(super.getHeaders(data), headers);
		// add default headers
		headers.add(CollectionUtil.newEntry("default-header-1", "value1"));
		headers.add(CollectionUtil.newEntry("default-header-2", "value2"));

		return headers;
	}

}
