// Copyright (c) 2022 Boomi, Inc.
package com.boomi.connector.sample.rest;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ContentType;
import com.boomi.connector.api.ObjectDefinition;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.util.BaseBrowser;
import com.boomi.util.ClassUtil;
import com.boomi.util.StreamUtil;

/**
 * Simple {@link Browser} implementation which supports a single "Pet" object.
 * The object definitions are created using a packaged schema.
 */
public class PetStoreBrowser extends BaseBrowser {

	protected PetStoreBrowser(BrowseContext context) {
		super(context);
	}

	@Override
	public ObjectTypes getObjectTypes() {
		return new ObjectTypes().withTypes(new ObjectType().withId("Pet"));
	}

	@Override
	public ObjectDefinitions getObjectDefinitions(String objectTypeId, Collection<ObjectDefinitionRole> roles) {
		String schema = getSchema();
		boolean supportsInput = PetStoreOperationType.fromContext(getContext()).supportsInput();
		ObjectDefinitions definitions = new ObjectDefinitions();
		for (ObjectDefinitionRole role : roles) {
			definitions.getDefinitions()
					.add(new ObjectDefinition().withElementName("/definitions/" + objectTypeId)
							.withInputType(((role == ObjectDefinitionRole.INPUT) && supportsInput) ? ContentType.JSON
									: ContentType.NONE)
							.withOutputType((role == ObjectDefinitionRole.OUTPUT) ? ContentType.JSON : ContentType.NONE)
							.withJsonSchema(schema));
		}
		return definitions;
	}

	/**
	 * Loads the packaged schema resource. 
	 * 
	 * @return string representation of the JSON schema 
	 */
	private static String getSchema() {
		try (InputStream input = ClassUtil.getResourceAsStream("schema.json")) {
			return StreamUtil.toString(input, "utf-8");
		} catch (IOException e) {
			throw new ConnectorException(e);
		}
	}

}
