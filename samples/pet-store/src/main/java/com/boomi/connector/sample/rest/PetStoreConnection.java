// Copyright (c) 2022 Boomi, Inc.
package com.boomi.connector.sample.rest;

import org.apache.http.entity.ContentType;

import com.boomi.common.rest.RestOperationConnection;
import com.boomi.connector.api.OperationContext;

/**
 * Simple {@link RestOperationConnection} implementation which overrides a few default behaviors. 
 */
public class PetStoreConnection extends RestOperationConnection {

    private final PetStoreOperationType _operationType; 
    
    /**
     * Creates a new instance with the provided context 
     * 
     * @param context
     */
    public PetStoreConnection(OperationContext context) {
        super(context);
        _operationType = PetStoreOperationType.fromContext(getContext()); 
    }

    /**
     * The content type for this connector is always application/json
     */
    @Override
    public ContentType getEntityContentType() {
        return ContentType.APPLICATION_JSON;
    }

    /**
     * The HTTP method is derived from the operation type instead of being the actual operation type  
     */
    @Override
    public String getHttpMethod() {
        return _operationType.getHttpMethod();
    }
    
    /**
     * Returns the configured operation type 
     * 
     * @return the operation type 
     */
    public PetStoreOperationType getOperationType() {
        return _operationType;
    }
}
