// Copyright (c) 2022 Boomi, Inc.
package com.boomi.connector.sample.rest;

import com.boomi.common.rest.RestConnector;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;

/**
 * Simple {@link RestConnector} implementation which overrides the default
 * operation type and introduces browse support.
 */
public class PetStoreConnector extends RestConnector {

	@Override
	public Browser createBrowser(BrowseContext context) {
		return new PetStoreBrowser(context);
	}

	@Override
	protected Operation createExecuteOperation(OperationContext context) {
		return new PetStoreOperation(new PetStoreConnection(context));
	}

}
