// Copyright (c) 2022 Boomi, Inc.
package com.boomi.connector.sample.rest;

import java.io.IOException;

import org.apache.http.HttpEntity;

import com.boomi.common.rest.RestOperation;
import com.boomi.connector.api.ObjectData;
import com.boomi.util.URLUtil;

/**
 * Simple {@link RestOperation} implementation which overrides a few default
 * behaviors.
 */
public class PetStoreOperation extends RestOperation {

	public PetStoreOperation(PetStoreConnection connection) {
		super(connection);
	}

	/**
	 * The path for this connector is based on the object type. Additionally, the
	 * GET operation includes the object id in the path.
	 */
	@Override
	protected String getPath(ObjectData data) {
		String path = getContext().getObjectTypeId().toLowerCase();
		if (getConnection().getOperationType() == PetStoreOperationType.GET) {
			return URLUtil.makeUrlString(path, data.getDynamicOperationProperties().getProperty("ID"));
		}
		return path;
	}

	/**
	 * Forces a null entity for operations that do not support input, defers to the
	 * default implementation otherwise.
	 */
	@Override
	protected HttpEntity getEntity(ObjectData data) throws IOException {
		if (getConnection().getOperationType().supportsInput()) {
			return super.getEntity(data);
		}
		return null;
	}
	
	@Override
	public PetStoreConnection getConnection() {
		return (PetStoreConnection) super.getConnection();
	}
}
