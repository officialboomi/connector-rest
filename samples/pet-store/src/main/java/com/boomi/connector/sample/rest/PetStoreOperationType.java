// Copyright (c) 2022 Boomi, Inc.
package com.boomi.connector.sample.rest;

import com.boomi.connector.api.BrowseContext;

/**
 * Simple enumeration of the custom operation types used by this connector.
 */
public enum PetStoreOperationType {

	ADD("POST"), GET("GET");

	private final String _httpMethod;

	PetStoreOperationType(String httpMethod) {
		_httpMethod = httpMethod;
	}

	/**
	 * Returns the HTTP Method used by this operation.
	 * 
	 * @return the HTTP method
	 */
	public String getHttpMethod() {
		return _httpMethod;
	}

	/**
	 * Indicates if the operation supports input. Operations that do not support
	 * input will not include a request profile during browse or an entity during
	 * execution.
	 * 
	 * @return true if the operation supports input, false otherwise 
	 */
	public boolean supportsInput() {
		return (this != GET);
	}

	/**
	 * Convenience method to derive an instance from the context
	 *  
	 * @param context the browse/operation context 
	 * @return the operation instance 
	 */
	public static PetStoreOperationType fromContext(BrowseContext context) {
		return valueOf(context.getCustomOperationType());
	}
}
